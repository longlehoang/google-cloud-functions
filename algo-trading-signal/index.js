/**
 * Background Cloud Function to be triggered by Pub/Sub.
 * This function is exported by index.js, and executed when
 * the trigger topic receives a message.
 *
 * @param {object} data The event payload.
 * @param {object} context The event metadata.
 */

exports.futAlgoSignal_PubSubToHangoutsChat = (event, context) => {
    const pubSubMessage = event.data;
    const messageStr = Buffer.from(pubSubMessage, 'base64').toString();
    //const signalEvent = JSON.parse(Buffer.from(pubSubMessage, 'base64').toString());
    //if (signalEvent.match_price == 0 || stkPrice.match_prices < stkPrice.floor_price || stkPrice.match_price > stkPrice.ceiling_price) return;
    
    //let date = new Date(Number.parseInt(signalEvent.timestamp));
    //dateStr = date.toLocaleTimeString("vi", {timeZone: "Asia/Saigon"});
    
    //var message = {'text': `Mã ${stkPrice.code} tăng ${chgText}%, giá khớp ${Number.parseFloat(matchPrice).toFixed(2)} vào lúc ${dateStr}`};
    var message = {'text': `${messageStr}`};
    postHangoutChatMsg(JSON.stringify(message));
};

function precise(x) {
  return Number.parseFloat(x).toPrecision(2);
}

function postHangoutChatMsg(message_data) {
    const https = require('https');

    const options = {
        hostname: 'chat.googleapis.com',
        port: 443,
        path: '/v1/spaces/AAAAJOy9Ztc/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=rstqbJjov25x5eolf1ah2cvitpP3n1C1nC0snd4wzyY%3D',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        }
    };

    const req = https.request(options, (res) => {
      console.log(`statusCode: ${res.statusCode}`)

      res.on('data', (d) => {
        process.stdout.write(d);
      })
    });

    req.on('error', (error) => {
      console.error(error);
    });

    req.write(message_data);
    req.end();  
};
