import json
import base64
import re
import dateutil.parser
import dateutil.tz
from google.cloud import firestore

vn_tz = dateutil.tz.tzoffset('UTC+7', 7 * 3600)


class EventType(object):
    SIGNAL = "SIGNAL"
    TRADING_ACTION = "TRADE"
    NOTIFICATION = "NOTIFICATION"
    UPDATE = "UPDATE"
    TTL = "TTL"
    SYSTEM = "SYSTEM"


# [START functions_handle_message]
def handle_events(event, context):
    """Background Cloud Function to be triggered by Pub/Sub.
    Args:
         event (dict):  The dictionary with data specific to this type of
         event. The `data` field contains the PubsubMessage message. The
         `attributes` field will contain custom attributes if there are any.
         context (google.cloud.functions.Context): The Cloud Functions event
         metadata. The `event_id` field contains the Pub/Sub message ID. The
         `timestamp` field contains the publish time.
    """

    # print("""This Function was triggered by messageId {} published at {}
    # """.format(context.event_id, context.timestamp))
    message_text = None
    if 'data' in event:
        event_data = base64.b64decode(event['data']).decode('utf-8')
        event_dict = json.loads(event_data)
        print("Received event: {}".format(event_data))

        db = firestore.Client()
        db.collection(u'messages').add(
            {u"created_at": firestore.SERVER_TIMESTAMP,
             u"created_by": event_dict['created_by'],
             u"pubsub_id": context.event_id,
             u"pubsub_timestamp": dateutil.parser.parse(context.timestamp),
             u"payload": event_data}
        )

        # True if all fields found in event_dict keys
        if 'event_type' in event_dict:
            p = re.compile("chat.googleapis.com\/v1\/spaces\/(.*?)\/messages")

            event_time = dateutil.parser.parse(
                event_dict["event_time"]).astimezone(vn_tz)
            date_text = event_time.date().strftime('%Y%m%d')
            time_text = event_time.time().strftime('%H:%M:%S')

            if event_dict['event_type'] == EventType.SIGNAL:
                weight_display = float(event_dict["weight"]) * 100
                message_text = "[SIGNAL] *{data[side]}* {data[price]} qty *{pct:.0f}%* at {time}".format(
                    data=event_dict, pct=weight_display, time=time_text)
            elif event_dict['event_type'] == EventType.TRADING_ACTION:
                message_text = "[TRADE] {{ {data[side]} | *{data[quantity]}* | *{data[avg_price]}*" \
                               " | {data[symbol]} }} by {data[sub_account]}" \
                    .format(data=event_dict)
            elif event_dict['event_type'] == EventType.UPDATE:
                message_text = "[UPDATE] RETURN *{0:.2f}%*".format(event_dict["today_return_pct"])
            elif event_dict['event_type'] in (EventType.SYSTEM, EventType.TTL, EventType.NOTIFICATION):
                message_text = "[{data[event_type]}] {data[content]}".format(data=event_dict)
            else:
                print("Invalid message {}".format(event_data))
                return

            bot_ref = db.collection(u'bots').document(event_dict['created_by'])
            bot_ss = bot_ref.get()
            if bot_ss.exists:
                bot_data = bot_ss.to_dict()
                for key in bot_data['subscriptions']:
                    url = bot_data['subscriptions'][key]
                    room_id = p.findall(url)[0]
                    session_doc_id = date_text + '-' + room_id
                    print("session_doc_id {}".format(session_doc_id))

                    session_ref = db.collection(u'sessions').document(session_doc_id)
                    session_ss = session_ref.get()

                    thread_name = ''
                    if session_ss.exists:
                        session_data = session_ss.to_dict()
                        thread_name = session_data['hangouts_chat_thread']
                    else:
                        session_ref.create({"last_activity": firestore.SERVER_TIMESTAMP})

                    resp = post_message(message=message_text, room_url=url, thread=thread_name)

                    if thread_name == '' and resp is not None:
                        resp_dict = json.loads(resp)
                        print("Thread {}".format(resp_dict["thread"]["name"]))
                        result = session_ref.update(
                            field_updates={"last_activity": firestore.SERVER_TIMESTAMP,
                                           "hangouts_chat_thread": resp_dict["thread"]["name"]}
                        )
                    elif resp is None:
                        print('Can not post message.')
            else:
                print("No subscription")

    else:
        print("No message data")
        return


# [END functions_handle_message]


def post_message(message, room_url, thread):
    import requests

    url = room_url
    bot_message = {'text': message, "thread": {"name": thread}}
    message_headers = {'Content-Type': 'application/json; charset=UTF-8'}

    response = requests.post(url=url,
                             data=json.dumps(bot_message),
                             headers=message_headers)
    if response.status_code == 200:
        print("Message sent!")
        return response.text
    else:
        print("An error has occurred.")
        return None
