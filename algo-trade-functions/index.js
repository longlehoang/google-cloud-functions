/**
 * Triggered from a message on a Cloud Pub/Sub topic.
 *
 * @param {!Object} event Event payload.
 * @param {!Object} context Metadata for the event.
 */
'use strict';

const { Firestore } = require('@google-cloud/firestore');

// Create a new client
const firestore = new Firestore();

const EventType = {
  SIGNAL: "SIGNAL",
  TRADING_ACTION: "TRADE",
  NOTIFICATION: "NOTIFICATION",
  UPDATE: "UPDATE",
  TTL: "TTL",
  SYSTEM: "SYSTEM"
};


exports.handleSignals = (event, context) => {
  const pubsubMessage = event.data;
  const message = Buffer.from(pubsubMessage, 'base64').toString();
  const msg_data = JSON.parse(message);
  console.log("Start handling message:", message);

  const BOT = typeof process.env.BOT_CODE !== 'undefined' ? process.env.BOT_CODE : '';
  const spacePattern = new RegExp("chat.googleapis.com\/v1\/spaces\/(.*?)\/messages");

  const botsColRef = firestore.collection("bots");
  const sessColRef = firestore.collection("sessions");

  try {
    var event_time = new Date(msg_data["event_time"]);
    const event_date_str = formatShortDate(event_time);
    const event_time_str = formatShortTime(event_time);

    // Construct message to send to chat room
    var publish_message = ''
    if (msg_data["event_type"] == EventType.SIGNAL) {
      let percent_purchase = Number.parseFloat(msg_data["weight"]) * 100;
      publish_message = `[SIGNAL] *${msg_data["side"]}* ${Number.parseFloat(msg_data["price"]).toFixed(2)} qty *${percent_purchase}%* at ${event_time_str}`;

    } else if (msg_data["event_type"] == EventType.TRADING_ACTION) {
      publish_message = `[TRADE] Traded by ${msg_data["sub_account"]} as { ${msg_data["side"]}-*${msg_data["quantity"]}*-${msg_data["symbol"]}-*${msg_data["avg_price"]}*-${msg_data["status"]} }`;

    } else if (msg_data["event_type"] == EventType.UPDATE) {
      publish_message = `[UPDATE] RETURN *${Number.parseFloat(msg_data["today_return_pct"]).toFixed(2)}%*`;

    } else if (msg_data["event_type"] == EventType.TTL || msg_data["event_type"] == EventType.SYSTEM || msg_data["event_type"] == EventType.NOTIFICATION) {
      publish_message = `[${msg_data["event_type"]}] ${msg_data["content"]}`;

    } else {
      // Unexpected message, quit the function
      console.warn(message);
      return;
    }

    msg_data["event_time"] = Firestore.Timestamp.fromDate(new Date(msg_data["event_time"]));
    msg_data["created_at"] = Firestore.Timestamp.fromDate(new Date(msg_data["created_at"]));

    botsColRef.doc(msg_data["created_by"])
      .get()
      .then((botSnapshot) => {
        let subs = [];
        if (botSnapshot.exists) {
          const subscriptionMap = botSnapshot.get("subscriptions") ? botSnapshot.get("subscriptions") : {};
          console.log(`Get subscription info: ${JSON.stringify(subscriptionMap)}`);

          // Create new session document for each subscriptions
          Object.keys(subscriptionMap).forEach(
            (key) => {
              console.log("Found key: ", key);

              let newSess = {
                "bot_id": msg_data["created_by"],
                "trade_date": event_date_str,
                "hangouts_chat_url": subscriptionMap[key]
              };
              subs.push(newSess);
            }
          );
          console.log("Retrieved ", subs.length, " subscriptions: ", JSON.stringify(subs));
        } else {
          console.log(`No subscription for [${msg_data["created_by"]}]`);
          return Promise.reject("No subscriptions found");
        }

        return Promise.resolve(subs);
      }, reject => {
        console.error("Reject while finding bots", reject);
        return null;
      }).then(values => {
        return Promise.all(
          values.map(async (value) => {
            try {
              let hcSpace = spacePattern.exec(value["hangouts_chat_url"])[1];
              let sessionId = `${value["trade_date"]}-${hcSpace}`;
              let hcThread = '';
              let sessDocRef = sessColRef.doc(sessionId);
              console.log(`Session: ${sessDocRef.id}`);
              value["session_id"] = sessionId;
              value["hangouts_chat_thread"] = "";

              let sessDocSnapshot = await sessDocRef.get();
              if (sessDocSnapshot.exists) {
                let sessDocData = sessDocSnapshot.data();
                try {
                  hcThread = sessDocData["hangouts_chat_thread"];
                } catch (e) {
                  hcThread = '';
                  console.warn("Getting Chat thread:", e);
                }
                value["hangouts_chat_thread"] = hcThread;
              } else {
                console.log("No thread found!!!");
              }
              console.log("Data for nofifying:", JSON.stringify(value));
            } catch (error) {
              console.error("Something wrong while querying for Chat thread", error);
              return null;
            }

            return value;
          })
        );
      }, reject => {
        console.error("Reject while retrieving subscriptions", reject);
        return null;
      }).then((values) => {
        return Promise.all(
          values.map(async (value) => {
            return await createMessage(value, publish_message);
          })
        );
      }, reject => {
        console.error("Reject while getting Chat thread", reject);
        return null;
      }).then((responses) => {
        // Get a new write batch
        let batch = firestore.batch();

        responses.map((resp) => {
          console.log(`Response from Chat: ${JSON.stringify(resp)}`);
          try {
            if (resp["hangouts_chat_thread"].length > 0) {
              console.log("Session already has Chat thread!");
            } else {
              let sessDocRef = sessColRef.doc(resp["session_id"]);
              batch.set(sessDocRef, {
                "hangouts_chat_thread": resp["thread"]["name"],
                "last_activity": Firestore.FieldValue.serverTimestamp()
              }, { merge: true });
            }
          } catch (error) {
            let sessDocRef = sessColRef.doc(resp["session_id"]);
            batch.set(sessDocRef, {
              "hangouts_chat_thread": resp["thread"]["name"],
              "last_activity": Firestore.FieldValue.serverTimestamp()
            }, { merge: true });
          }
        });

        // Bulk update threads for all subscriptions
        batch.commit().then(() => {
          console.log("Sessions successfully written!");
        }).catch(error => {
          console.error("Error writing sessions: ", error);
        });

      }, reject => {
        console.error("Reject while calling http request", reject);
        return null;
      }).catch(err => {
        console.error("Error while executing: ", err);
      });

    // Save message to firestore
    firestore.collection("messages").add({
      "created_at": Firestore.FieldValue.serverTimestamp(),
      "created_by": msg_data["created_by"],
      "payload": message
    }).then((docRef) => {
      docRef.get().then(docSnap => {
        console.log("Inserted to messages: ", docSnap.id);
      });
    }).catch(err => {
      console.error("While inserting messages: ", err);
    });

  } catch (err) {
    console.error("Function error: ", err);
  }
}

/********************************************************
 * Post message to Hangouts Chat
 * 
 ********************************************************/
function createMessage(data, message) {
  const https = require('https');
  return new Promise((resolve, reject) => {
    console.log(`Post message ${message}`);
    const options = {
      host: 'chat.googleapis.com',
      port: 443,
      path: data["hangouts_chat_url"],
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8'
      }
    };
    console.log(`Request JSON: ${JSON.stringify(options)}`);
    // Construct message to be sent
    var bot_message = { "text": message };

    if (typeof data["hangouts_chat_thread"] !== "undefined" && data["hangouts_chat_thread"] != '') {
      bot_message["thread"] = { "name": data["hangouts_chat_thread"] };
    } else {
      data["hangouts_chat_thread"] = "";
    }

    // Make a http request to post message to Hangouts Chat
    const req = https.request(options, (res) => {
      var { statusCode } = res;
      var contentType = res.headers['content-type'];

      res.setEncoding('utf8');
      let error;

      if (statusCode !== 200) {
        error = new Error('Request Failed.\n' +
          `Status Code: ${statusCode}`);
      } else if (!/^application\/json/.test(contentType)) {
        error = new Error('Invalid content-type.\n' +
          `Expected application/json but received ${contentType}`);
      }

      if (error) {
        console.error("Unexpected HTTP response: ", error);
        reject(error);
      }

      let res_str = '';

      res.on('data', (chunk) => {
        res_str += chunk;
      });

      res.on('end', () => {
        // console.log(`Reponse data: ${res_str}`);
        try {
          const parsedData = JSON.parse(res_str);
          parsedData["session_id"] = data["session_id"];
          parsedData["hangouts_chat_thread"] = data["hangouts_chat_thread"];
          resolve(parsedData);
        } catch (e) {
          console.error("Error while handling response json:", e);
          reject(e);
        }
      });
    });

    req.on('error', (error) => {
      console.error("HTTP request: ", error);
      reject(error);
    });

    console.log(`Sending message data ${JSON.stringify(bot_message)}`)
    req.write(JSON.stringify(bot_message));
    req.end();
  });
}


/********************************************************
 * Format date as YYYYMMDD
 * 
 ********************************************************/
function formatShortDate(date) {
  const moment = require('moment-timezone');
  var wrapped = moment(new Date(date)).tz('Asia/Saigon');
  return wrapped.format('YYYYMMDD');
}

/********************************************************
 * Format date as YYYY-MM-DD
 * 
 ********************************************************/
function formatDate(date) {
  const moment = require('moment-timezone');
  var wrapped = moment(new Date(date)).tz('Asia/Saigon');
  return wrapped.format('YYYY-MM-DD');
}

/********************************************************
 * Format time as HH:mm:ss
 * 
 ********************************************************/
function formatShortTime(date) {
  const moment = require('moment-timezone');
  var wrapped = moment(new Date(date)).tz('Asia/Saigon');
  return wrapped.format('HH:mm:ss');
}

/********************************************************
 * Format datetime as YYYYMMDD HH:mm:ss
 * 
 ********************************************************/
function formatShortDatetime(datetime) {
  const moment = require('moment-timezone');
  var wrapped = moment(new Date(datetime)).tz('Asia/Saigon');
  return wrapped.format("YYYYMMDD HH:mm:ss");
}

/********************************************************
 * Format datetime as YYYY-MM-DD HH:mm:ss
 * 
 ********************************************************/
function formatDatetime(datetime) {
  const moment = require('moment-timezone');
  var wrapped = moment(new Date(datetime)).tz('Asia/Saigon');
  return wrapped.format('YYYY-MM-DD HH:mm:ss');
}