from httplib2 import Http
import json
import logging
import mandrill
import re
import os
from time import sleep
from flask import Response

TEST_MANDRILL_API_KEY = 'bU01HLsq1Lz6FVkrbT-LdA'
#Space can be DM or Room
HANGOUTS_CHAT_SPACE_ID = 'AAAAV8AIj6c'
HANGOUTS_CHAT_KEY = 'AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI'
HANGOUTS_CHAT_TOKEN = '4tuDuLQYK5kgCc2cKfGKNzX7swmR-cXjgBgu-GSmYSg%3D'

def handleIncomingMessage(request):
    """Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/0.12/api/#flask.Flask.make_response>`.
    """
    if request.method == 'POST':
        events = request.form.get('mandrill_events')
        events_json = json.loads(events)
        #Wait for API data ready
        sleep(30)
        for event in events_json:
            logging.info(json.dumps(event))
            msg_id = event['_id']
            msg = getEmailContent(msg_id)
            postHangoutsChatMsg(msg)
    return Response('OK', status=200)

#
# Hangouts Chat incoming webhook quickstart
#
def postHangoutsChatMsg(account_id):
    chat_space_id = os.environ.get('HANGOUTS_CHAT_SPACE_ID', HANGOUTS_CHAT_SPACE_ID)
    chat_key = os.environ.get('HANGOUTS_CHAT_KEY', HANGOUTS_CHAT_SPACE_ID)
    chat_token = os.environ.get('HANGOUTS_CHAT_TOKEN', HANGOUTS_CHAT_SPACE_ID)
    
    url = 'https://chat.googleapis.com/v1/spaces/{}/messages?key={}&token={}'.format(chat_space_id,chat_key,chat_token)
    logging.info('Pure message: %s' % account_id)
    if (account_id):
        msg_text = u'Mật khẩu giao dịch qua Tổng đài đã được chuyển đến KH có TK[*%s*]'%account_id
    else:
        logging.info('Cannot send message: %s' % account_id)
        return
    bot_message = {
        'text' : msg_text}

    message_headers = { 'Content-Type': 'application/json; charset=UTF-8'}

    http_obj = Http()

    response = http_obj.request(
        uri=url,
        method='POST',
        headers=message_headers,
        body=json.dumps(bot_message),
    )
    
def getEmailContent(msg_id):
    try:
        # Get Prod API Key, otherwise use test key instead.
        api_key = os.environ.get('MANDRILL_API_KEY', TEST_MANDRILL_API_KEY)
        
        mandrill_client = mandrill.Mandrill(api_key)
        result = mandrill_client.messages.content(id=msg_id)
        content_txt = result['text']
        logging.info('Successfully get content for message %s' % msg_id)
        match = re.search('(?<=006)\w+\d{6}', content_txt)
        account_id = ''
        if (match is not None):
            account_id = '006%s'%match.group(0)
    except (mandrill.Error) as e:
        # Mandrill errors are thrown as exceptions
        # A mandrill error occurred: <class 'mandrill.UnknownMessageError'> - No message exists with the id 'McyuzyCS5M3bubeGPP-XVA'
        logging.error('A mandrill error occurred: %s - %s' % (e.__class__, e))
    else:
        return account_id
